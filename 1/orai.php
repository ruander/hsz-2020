<?php
//egysoros megjegyzés
/*
 Több
soros
megjegyzés
 */
//ez egy PURE php fájl, azaz csak PHP script van benne
//Kiírás a 'standard outputra' stdo ( fileba ahol éppen kiíírunk)
print "ez egy szöveg";//operátor: "", '' -> string határoló operátor
echo "<br>Ez egy szöveg új sorban...";//operátor ; -> utasítás lezárása

//változók - primitívek
$szam = 214;//operátor $ -> változó | = -> értékadó operátor | tipus: integer (int)
$szoveg = 'Horváth György';//tipus -> string
$tort = 6/7;//tipus -> floating point (float) - lebegő pontos szám
$logikai = true;//tipus -> boolean (bool)

//változó dump szebb formában
echo "<pre>";
//fejlesztés közben info a változó(k)ról
var_dump($szam,$szoveg,$tort,$logikai);
echo "</pre>";

//kiíráskor, minde automatikusan stringgé alakul ha képes rá
echo 'A $szam értéke: '.$szam;//operátor: . -> konkatenátor
echo "<br>A $szoveg értéke: ".$szoveg;//"" belül a változó értéke jelenik meg
echo "<br>A \$szoveg \"idézet\" értéke: $szoveg";// \ operátor: -> escape - kilépteti a nyelvi elemek közül a közv utána található karaktert

//változók, tömbök
$tomb = array(131, 7/9, true, 'valami szöveg');
$tomb = [134,'dfss',true];//változó újradeklrálás, felülírás

$tomb = [];//tömb ürítése v. üres tömb létrehozása
$tomb = ['a', 12, 4*562 ];
echo "<pre>";
var_dump($tomb);
echo "</pre>";
//érték hozzáadása irányított tömb indexre
$tomb[100] = 'Helo';
echo "<pre>";
var_dump($tomb);
echo "</pre>";
//érték hozzáadása asszociatív indexre
$tomb['email'] = 'hgy@iworkshop.hu';
echo "<pre>";
var_dump($tomb);
echo "</pre>";
//tömb értéke lehet nem primitív is
$tomb['beagyazott'] = $tomb;
echo "<pre>";
var_dump($tomb);
echo "</pre>";