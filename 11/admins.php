<?php
//ADMINS CRUD megvalósítás
require 'connect.php';//db csatlakozás betöltése
require_once 'functions.php';//saját eljárások

//admins tábla kezelése
//listázzuk ki a rekordokat és adjunk lehetőséget új rekord (adatok) felvitelére (lista - táblázat)
//form - create
//form - update (id)
//törlés lehetőség (id)

//erőforrások
$action = filter_input(INPUT_GET, 'action') ?: 'list';//most legyen beállítva listázásra
//azonosító feldolgozása urlből
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//vagy kapunk egész szám tid, vagy null a változó értéke
/*
echo '<pre>';
var_dump(filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT));
var_dump(filter_input(INPUT_GET, 'tid'));
echo gettype($_GET['tid']);
var_dump(0);
var_dump(is_int('0'));
var_dump(null, false, '' , 1, true);
if( !$valami  ){ ... }*/

$output = '';//ide gyűjtjük a kiírandó stringeket (html)   a
if (!empty($_POST)) {
    //hibakezelés
    $hiba = [];
    //Név - minimum 3 karakter
    $name = filter_input(INPUT_POST, 'fullName');

    $name = trim($name);//spacek eltávolítása - mindkét végéről (ltrim,rtrim)

    $name = strip_tags($name);//html tagek takarítása
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['fullName'] = '<span class="error">Minimum 3 karakter!</span>';
    }
    //echo $name. ' hossza: '.mb_strlen($name,'utf-8');//szőveg hossza, ékezetes betűt jól számolja, az strlen() nem, ő->2byte

    //Email - legyen email, és ne legyen már foglalt a DB-ben, mert unique mező
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    //ha nincs email, akkor hiba,és ne legyen foglalt
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    } else {//ha formailag jó, foglalt-e
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";//lekérés összeállítása
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $rowAdmin = mysqli_fetch_row($result);

        //ha módósítunk és a kapott azonosító az email alapján az adatbázisból az éppen módosítandó admin azonosítója, akkor az nem hiba
        if (
            !empty($rowAdmin) && //ha találunk az adott emailhez tartozó azonosítót, és
            ($tid == false || $tid != $rowAdmin[0]) //nincs tid tehát nem módosítunk VAGY nem egyezik a modosítandó és talált azonosító
        ) {
            $hiba['email'] = '<span class="error">Már foglalt email!</span>';
        }
    }

    //jelszó 1 és 2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    if ($action == 'create' || $password != '') {//ha uj felvitel van mindenképp vagy ha modositunk és gépeltek akár 1 karaktert az 1. jelszó mezőbe

        //1.
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['password'] = '<span class="error">Legalább 6 karakter!</span>';
        } elseif ($password !== $repassword) {//2. nem egyeznek a jelszavak
            $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            //kódoljuk el a jelszót
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }
    //státusz
    $status = filter_input(INPUT_POST, 'status') ?: 0;//ha nem sikerül leszűrni legyen 0


    //adatvédelmi új felvitel esetén
    //echo '<pre>'.var_export($_POST,true).'</pre>';
    if (!filter_input(INPUT_POST, 'terms') && $action == 'create') {
        $hiba['terms'] = '<span class="error">Kötelezö kipipálni!</span>';
    }


    if (empty($hiba)) {
        //adatok rendberakása
        $data = [
            'fullName' => $name,
            'email' => $email,
            'status' => $status,
        ];
        //jelszó akkor legyen a tömbben ha van tartalma
        if ($password != '') {
            $data['password'] = $password;
        }

        //időbélyegek és művelet
        if ($action == 'create') {
            $data['time_created'] = date('Y-m-d H:i:s');
            //insert
            $qry = "INSERT INTO 
                        admins(
                               `fullname`,
                               `email`,
                               `password`,
                               `status`,
                               `time_created`
                               )
                        VALUES(
                               '{$data['fullName']}',
                               '{$data['email']}',
                               '{$data['password']}',
                               '{$data['status']}',
                               '{$data['time_created']}')";
        } else {//update
            $data['time_updated'] = date('Y-m-d H:i:s');
            //update
            $qry = "UPDATE admins SET 
                        `fullname` = '{$data['fullName']}',
                        `email` = '{$data['email']}', 
                        `status` = '{$data['status']}',";
            //jelszó csak akkor kell ha van tartalma
            if (isset($data['password'])) {
                $qry .= "`password` = '{$data['password']}',";
            }

            $qry .= "`time_updated` = '{$data['time_updated']}'
                     WHERE id = $tid;
                     ";
            //echo $qry;
        }
        //lekérés futtatása vagy állj
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //var_dump(mysqli_insert_id($link));die();
        //átirányítás a listára
        header('location:'.$_SERVER['PHP_SELF']);
        //echo '<pre>' . var_export($data, true) . '</pre>';
        exit();

    }
}

switch ($action) {
    case 'delete':
        //törlünk(id)

            //kaptunk id-t
            //echo 'törlünk - id:' . $tid;
            //törlünk
            mysqli_query($link, "DELETE FROM admins WHERE id  = '$tid' LIMIT 1") or die(mysqli_error($link));
            //vissza a listára
            header('location:'.$_SERVER['PHP_SELF']);
            exit();

        break;
    case 'update':
        //form - módosítunk(id)
        if ($tid) {
            //kaptunk id-t

            $qry = "SELECT fullName,email,status FROM admins WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $adminRow = mysqli_fetch_assoc($result);
            echo '<pre>' . var_export($adminRow, true) . '</pre>';
            //update űrlap
            $title = "<h2>Admin módosítása [--$tid--]</h2>";//cím
            $btnTitle = "Módosítok";//gomb felirat

        } else {
            //nem kaptunk idt
        }
    //break;
    case 'create':
        $adminRow = isset($adminRow) ? $adminRow : [];

        //form - üres - új felvitel
        $title = isset($title) ? $title : '<h1>Regisztráció</h1>';//ha már van, legyen az, ha nincs legyen az új
        $btnTitle = isset($btnTitle) ? $btnTitle : 'Regisztráció';//...

        //fűzzük az outputhoz a címet
        $output .= $title;

        $output .= '<a href="?action=list">vissza a listához</a>';//vissza gomb
        $form = '<form method="post">';//form nyitása

        //NÉV beviteli mező
        $form .= '<label>
                        <span>Név<sup>*</sup></span>
                        <input type="text" name="fullName" placeholder="Teszt Elek" value="' . getValue('fullName', $adminRow) . '">';//label nyitás, mező neve és beviteli mező
        //hibaüzenet ha kell
        $form .= getError('fullName');
        $form .= '</label>';//label zárás

        //Email beviteli mező
        $form .= '<label>
                        <span>Email<sup>*</sup></span>
                        <input type="text" name="email" placeholder="email@cim.hu" value="' . getValue('email', $adminRow) . '">';//label nyitás, mező neve és beviteli mező
        //hibaüzenet ha kell
        $form .= getError('email');
        $form .= '</label>';//label zárás

        //Jelszó 1 beviteli mező
        $form .= '<label>
                        <span>Jelszó<sup>*</sup></span>
                        <input type="password" name="password" placeholder="******" value="">';//label nyitás, mező neve és beviteli mező
        //hibaüzenet ha kell
        $form .= getError('password');
        $form .= '</label>';//label zárás

        //Jelszó 2 beviteli mező
        $form .= '<label>
                        <span>Jelszó ismétlése<sup>*</sup></span>
                        <input type="password" name="repassword" placeholder="******" value="">';//label nyitás, mező neve és beviteli mező
        //hibaüzenet ha kell
        $form .= getError('repassword');
        $form .= '</label>';//label zárás


        // teszt: aktiv felhasználó statusz inaktivra és máshol hiba a formon, maradjon a checkbox inaktivan
        if ($action == 'create') {
            //Adatvédelmi tájékoztató checkbox
            $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';//ki volt e pipálva
            $form .= '<label>
                  <span><input type="checkbox" name="terms" value="1" ' . $checked . '> Elolvastam és megértettem az <a href="#" target="_blank">Adatvédelmi tájékoztató</a>ban leírtakat!</span>';
            $form .= getError('terms');//hibaüzenet a hibatömbből, saját eljárással
            $form .= '</label>';//label zárás
        } else {
            //update
            //az adatbáisban 1 statusu felh. statusa 0 állítva (kivesszük a pipát) és egyébként máshol (jelszó1) hibásan elküldve  maradjon üresen
            //
            /*            if (
                            filter_input(INPUT_POST, 'status')
                            || //or
                            $adminRow['status'] == 1 && empty($_POST) //and
                        ) {

                            $checked = 'checked';

                        } else {
                            $checked = '';
                        };//ki volt e pipálva
                        */
            $checked = (filter_input(INPUT_POST, 'status')
                || //or
                $adminRow['status'] == 1 && empty($_POST)) ? 'checked' : '';

            $form .= '<label>
                  <span><input type="checkbox" name="status" value="1" ' . $checked . '> status</span>';
            $form .= '</label>';//label zárás

        }


        $form .= "<button>$btnTitle</button></form>";//gomb és form zárás

        $output .= $form;//tegyük az outputhoz az űrlapot
        break;
    default:
        //listázás

        $qry = "SELECT id, fullname, email, status FROM admins";//lekérés összeösszeállítása
        $results = mysqli_query($link, $qry) or die(mysqli_error($link));
        //új felvitel opció
        $output .= '<a href="?action=create">Új felvitel</a>';
        //táblázat felépítése
        $output .= '<table border="1">';
        //fejléc
        $output .= '<tr>
                        <th>id</th>
                        <th>név</th>
                        <th>email</th>
                        <th>status</th>
                        <th>művelet</th>
                    </tr>';
        //sorok
        while ($row = mysqli_fetch_assoc($results)) {
            $output .= '<tr class="data-row">
                            <td>' . $row["id"] . '</td>
                            <td>' . $row["fullname"] . '</td>
                            <td>' . $row["email"] . '</td>
                            <td>' . $row["status"] . '</td>
                            <td><a class="btn btn-warning btn-update" href="?action=update&amp;tid=' . $row["id"] . '">módosít</a> | <a onclick="return confirm(\'Biztosan törlöd?\');"  class="btn btn-danger btn-delete" href="?action=delete&amp;tid=' . $row["id"] . '">törlés</a></td>
                        </tr>';
        }
        $output .= '</table>';

        break;
}

echo $output;//output kiírása 1 lépésben

$css = '<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
form {
padding: 15px;
max-width:450px;
margin: 0 auto;
}
label {
margin: 5px auto 15px;
width: 100%;
display:flex;
flex-flow:column;
}
.error {
color:#f00;
font-style: italic;
font-size:0.7em;
}
</style>';

echo $css;