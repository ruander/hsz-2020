<?php
//saját eljárások gyüjteménye

/**
 * Input mezők value értékét adja vissza (text)
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName, $rowdata = []){
    $ret = false;
    if(filter_input(INPUT_POST, $fieldName) !== NULL){//ha van ilyen eleme a postnak , azzal fogunk visszatérni
        $ret = filter_input(INPUT_POST, $fieldName);

    }elseif( isset($rowdata[$fieldName]) ){//ha a kapott tömbben (DB) van ilyen elem, azzal fogunk visszatérni

        $ret = $rowdata[$fieldName];
    }

    return $ret;
}

/**
 * Hibakiíró eljárás, kell hozzá egy $hiba tömb, amiben a mező neve kulcson található hibaüzenet, ha az van
 * @param $fieldName
 * @return false|mixed
 */
function getError($fieldName){
    global $hiba;//hibatömb legyen látható az eljárás számára

    if(isset($hiba[$fieldName])){//ha van ilyen elemünk, térjünk vele vissza
        return $hiba[$fieldName];
    }
    //minden más esetben térjünk vissza falseal
    return false;
}