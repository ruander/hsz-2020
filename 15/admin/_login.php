<?php
include '../config/connect.php';//adatbázis csatlakozás
include '../config/functions.php';//saját eljárások
include '../config/settings.php';//beállítások
//munkafolyamat
//var_dump($_SESSION);
session_start();//mf indítása
var_dump($_SESSION);
if(auth()){
    //átirányítás az indexre
    header("location:index.php");
    exit();
}

$info = '<span class="info">Kérjük írja be az azonosító adatokat</span>';
if (!empty($_POST)) {
    if(login()){
        //átirányítás az indexre
        header("location:index.php");
        exit();
    } else {
        $info = '<span class="error">Nem érvényes email/jelszó páros!</span>';
    }

}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Adminisztráció - Belépés</title>
</head>
<body>
<form method="post">
    <h1>Adminisztráció - Belépés</h1>
    <div class="message">
        <?php echo $info; ?>
    </div>
    <label>
        <span>Email</span>
        <input type="text" name="email" value="<?php echo getValue('email'); ?>" placeholder="email@cim.hu">
    </label>
    <label>
        <span>Jelszó</span>
        <input type="password" name="password" value="" placeholder="******">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>
