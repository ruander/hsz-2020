<?php
include '../config/connect.php';//adatbázis csatlakozás
include '../config/functions.php';//saját eljárások
include '../config/settings.php';//beállítások
session_start();//mf indítása
//melyik menüpont aktiv
$o = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;
$baseUrl = 'index.php?p=' . $o;//alap url kialakítása

/**
 * Ezekbe a változókba lehet gyűjteni a modulon belüli kiírandó elemeket
 */
$output = '';//output létrehozása, ehhez 'fűz' mindenki
$additionalCss = '';//additonalCss létrehozása, ehhez 'fűz' mindenki
$additonalJs = '';//additonalJs létrehozása, ehhez 'fűz' mindenki

//@todo: HF admin lte blank page oldaából átemelni a kinézetet + a menü eljárást módosítani a classokkal/szerkezettel + kilépés megoldgása
//kiléptetünk ha kell (beleroncsolunk a mf ba)
if (filter_input(INPUT_GET, 'logout') !== null) {
    logout();
}
//azonosítás
if (!auth()) {
    header('location:login.php');
    exit();
} else {
    $userbar = '<div class="userbar">Üdvözöllek ' . $_SESSION['userdata']['fullName'] . '! <a href="?logout">Kilépés</a></div>';
}

//modulok beállításai (alapok a settingsből) - ha nem 'nyultak bele az urlbe'
if (!isset($adminMenu[$o])) {//ha nem stimmel valami, azaz nincs a menutombunkbe a kivánt 'azonosító' akkor álljunk vissza 'alapra'
    header('location:index.php');
    exit();
}
$moduleFile = $moduleDir . $adminMenu[$o]['module'] . $moduleExt;
if (file_exists($moduleFile)) {
    include $moduleFile;//ha létezik, töltsük be (csak működés, $output és +cssek +javascriptek kialakítása)
}





//modulok betöltése
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=yes, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztrációs felület</title>
    <?php
    echo $additionalCss;
    ?>
</head>
<body>
<?php
echo $userbar;

echo adminMenu($adminMenu);//menü

//output a moduleból
echo $output;
?>
</body>
</html>
