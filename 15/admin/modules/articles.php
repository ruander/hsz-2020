<?php
//ARTICLES CRUD megvalósítás
if (!$link) {//önálló (rendszeren kívüli) file futtatás elleni védelem
    header('location:../index.php');
    exit();
}

//erőforrások
$action = filter_input(INPUT_GET, 'action') ?: 'list';//most legyen beállítva listázásra
//azonosító feldolgozása urlből
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//vagy kapunk egész szám tid, vagy null a változó értéke
$dbTable = 'articles';//ebbe a táblába dolgozunk


//$output = '';//ide gyűjtjük a kiírandó stringeket (html)   - ez már az index fileból jön ahol includeoljuk a modult
if (!empty($_POST)) {
    //hibakezelés
    $hiba = [];

    //státusz
    $status = filter_input(INPUT_POST, 'status') ?: 0;//ha nem sikerül leszűrni legyen 0


    if (empty($hiba)) {
        die('oké');
        //adatok rendberakása
        $data = [
            'fullName' => $name,
            'email' => $email,
            'status' => $status,
        ];


        //időbélyegek és művelet
        if ($action == 'create') {
            $data['time_created'] = date('Y-m-d H:i:s');
            //insert
            $qry = "INSERT INTO 
                        $dbTable(
                               `fullname`,
                               `email`,
                               `password`,
                               `status`,
                               `time_created`
                               )
                        VALUES(
                               '{$data['fullName']}',
                               '{$data['email']}',
                               '{$data['password']}',
                               '{$data['status']}',
                               '{$data['time_created']}')";
        } else {//update
            $data['time_updated'] = date('Y-m-d H:i:s');
            //update
            $qry = "UPDATE $dbTable SET 
                        `fullname` = '{$data['fullName']}',
                        `email` = '{$data['email']}', 
                        `status` = '{$data['status']}',";
            //jelszó csak akkor kell ha van tartalma
            if (isset($data['password'])) {
                $qry .= "`password` = '{$data['password']}',";
            }

            $qry .= "`time_updated` = '{$data['time_updated']}'
                     WHERE id = $tid;
                     ";
            //echo $qry;
        }
        //lekérés futtatása vagy állj
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //var_dump(mysqli_insert_id($link));die();
        //átirányítás a listára
        header('location:' . $baseUrl);
        //echo '<pre>' . var_export($data, true) . '</pre>';
        exit();

    }
}

switch ($action) {
    case 'delete':
        //törlünk(id)

        //kaptunk id-t
        //echo 'törlünk - id:' . $tid;
        //törlünk
        mysqli_query($link, "DELETE FROM $dbTable WHERE id  = '$tid' LIMIT 1") or die(mysqli_error($link));
        //vissza a listára
        header('location:' . $baseUrl);
        exit();

        break;
    case 'update':
        //form - módosítunk(id)
        if ($tid) {
            //kaptunk id-t

            $qry = "SELECT * FROM $dbTable WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $articleRow = mysqli_fetch_assoc($result);
            echo '<pre>' . var_export($articleRow, true) . '</pre>';
            //update űrlap
            $title = "<h2>Cikk módosítása [--$tid--]</h2>";//cím
            $btnTitle = "Módosítok";//gomb felirat

        } else {
            //nem kaptunk idt
        }
    //break;
    case 'create':
        $articleRow = isset($articleRow) ? $articleRow : [];

        //form - üres - új felvitel
        $title = isset($title) ? $title : '<h1>Új cikk felvitele</h1>';//ha már van, legyen az, ha nincs legyen az új
        $btnTitle = isset($btnTitle) ? $btnTitle : 'Új felvitel';//...

        //fűzzük az outputhoz a címet
        $output .= $title;

        $output .= '<a href="' . $baseUrl . '&amp;action=list">vissza a listához</a>';//vissza gomb
        $form = '<form method="post">';//form nyitása

        //Cikk címe beviteli mező
        $form .= '<label>
                        <span>Cím<sup>*</sup></span>
                        <input type="text" name="title" placeholder="Szuper cikk" value="' . getValue('title', $articleRow) . '">';//label nyitás, mező neve és beviteli mező
        //hibaüzenet ha kell
        $form .= getError('title');
        $form .= '</label>';//label zárás


        //státusz

        $checked = (
            filter_input(INPUT_POST, 'status')
            || //or
            empty($_POST) && isset($articleRow['status']) && $articleRow['status'] == 1
        ) ? 'checked' : '';

        $form .= '<label>
                  <span><input type="checkbox" name="status" value="1" ' . $checked . '> status</span>';
        $form .= '</label>';//label zárás


        $form .= "<button>$btnTitle</button></form>";//gomb és form zárás

        $output .= $form;//tegyük az outputhoz az űrlapot
        break;
    default:
        //listázás

        $qry = "SELECT id, title, author, status, time_published FROM $dbTable";//lekérés összeösszeállítása
        $results = mysqli_query($link, $qry) or die(mysqli_error($link));
        //új felvitel opció
        $output .= '<a href="' . $baseUrl . '&amp;action=create">Új felvitel</a>';
        //táblázat felépítése
        $output .= '<table border="1">';
        //fejléc
        $output .= '<tr>
                        <th>id</th>
                        <th>cím</th>
                        <th>szerző</th>
                        <th>status</th>
                        <th>publikálás dátuma</th>
                        <th>művelet</th>
                    </tr>';
        //sorok
        while ($row = mysqli_fetch_assoc($results)) {
            $output .= '<tr class="data-row">
                            <td>' . $row["id"] . '</td>
                            <td>' . $row["title"] . '</td>
                            <td>' . $row["author"] . '</td>
                            <td>' . $row["status"] . '</td>
                            <td>' . $row["time_published"] . '</td>
                            <td><a class="btn btn-warning btn-update" href="' . $baseUrl . '&amp;action=update&amp;tid=' . $row["id"] . '">módosít</a> | <a onclick="return confirm(\'Biztosan törlöd?\');"  class="btn btn-danger btn-delete" href="' . $baseUrl . '&amp;action=delete&amp;tid=' . $row["id"] . '">törlés</a></td>
                        </tr>';
        }
        $output .= '</table>';

        break;
}

//echo $output;//output kiírása 1 lépésben

$additionalCss = '<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
form {
padding: 15px;
max-width:450px;
margin: 0 auto;
}
label {
margin: 5px auto 15px;
width: 100%;
display:flex;
flex-flow:column;
}
.error {
color:#f00;
font-style: italic;
font-size:0.7em;
}
</style>';

//echo $css;