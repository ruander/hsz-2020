<?php
//saját eljárások gyüjteménye
/**
 * Admin menu render
 * @param array $adminMenu | admin menu tömb a settingsből
 * @return string
 */
function adminMenu ($adminMenu = []){
    //menü
    $menu = '<nav class="mainMenu">';
    $menu .= '<ul>';
    foreach($adminMenu as $id => $menuItem){//menüpontok
        $menu .= '<li><a href="?p='.$id.'">'.$menuItem['title'].'</a></li>';
    }
    $menu .= '</ul>';
    $menu .= '</nav>';

    return $menu;
}

/**
 * Beléptető eljárás
 * @return bool
 */
function login()
{
    global $secret_key, $link;//titkosító hasht és a db link-et lássa az eljárás

    //kapott email
    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));//query védelem
    //kapott jelszó
    $password = filter_input(INPUT_POST, 'password');

    $qry = "SELECT password,id,fullname 
                 FROM admins 
                 WHERE email = '$email' 
                 AND status = 1
                 LIMIT 1";//lekérjük a jelszót az emailhez
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $adminRow = mysqli_fetch_row($result);

    $checkPass = isset($adminRow[0]) ? password_verify($password, $adminRow[0]) : false;//jelszó ellenőrzés


    if ($checkPass) {
        //jelszó rendben
        //felhasználó adatait eltároljuk a mf tömbjébe
        $_SESSION['userdata'] = [
            'id' => $adminRow[1],
            'fullName' => $adminRow[2],
            'email' => $email,
        ];
        //mf alapján
        $sid = $_SESSION['id'] = session_id();
        $stime = time();//timestamp
        $spass = md5($adminRow[1] . $sid . $secret_key);//mf jelszó userId+session_id+karakterlánc
        //var_dump($sid,$spass,$stime);
        //beragadt munkafolyamat törlése
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid'") or die(mysqli_error($link));
        //bejelentkezés rögzítése a sessions táblába
        $qry = "INSERT 
                    INTO sessions(`sid`,`spass`,`stime`)
                    values('$sid','$spass','$stime')";
        mysqli_query($link, $qry) or die(mysqli_error($link));

        return true;
    }

    return false;
}

/**
 * Belépés érvényességének ellenőrzése
 * @return bool
 */
function auth()
{
    global $link, $secret_key;
    $sid = session_id();//mf. azonosító

//lejárt mf-ok törlése
    $now = time();
    $expired = $now - (15 * 60);//15 perc
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//sessionsben tárolt jelszó az adott mf-hoz
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    //var_dump($row);

    if(!isset($_SESSION['userdata']['id'])) return false;//ha ilyen nincs innen felesleges továbbmenni

    //legyártjuk a rendekezésre álló adatokból újra a jelszó
    $spass = md5($_SESSION['userdata']['id'] . $sid . $secret_key);
    if ( $row[0] === $spass) {
        //sikerült az azonosítás, frissítjük a timestampet (spass)
        mysqli_query($link,"UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }

    return false;
}

/**
 * Kiléptetés
 * void
 */
function logout(){
    global $link;

    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."'") or die(mysqli_error($link));
    $_SESSION = [];
}

/**
 * Input mezők value értékét adja vissza (text)
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName, $rowdata = []){
    $ret = false;
    if(filter_input(INPUT_POST, $fieldName) !== NULL){//ha van ilyen eleme a postnak , azzal fogunk visszatérni
        $ret = filter_input(INPUT_POST, $fieldName);

    }elseif( isset($rowdata[$fieldName]) ){//ha a kapott tömbben (DB) van ilyen elem, azzal fogunk visszatérni

        $ret = $rowdata[$fieldName];
    }

    return $ret;
}

/**
 * Hibakiíró eljárás, kell hozzá egy $hiba tömb, amiben a mező neve kulcson található hibaüzenet, ha az van
 * @param $fieldName
 * @return false|mixed
 */
function getError($fieldName){
    global $hiba;//hibatömb legyen látható az eljárás számára

    if(isset($hiba[$fieldName])){//ha van ilyen elemünk, térjünk vele vissza
        return $hiba[$fieldName];
    }
    //minden más esetben térjünk vissza falseal
    return false;
}


/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = ['à','á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' '];
    $good = ['a','a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-'];
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}