<?php
/**
 * Rendszerhez tartozó főbb beállítások
 */
$secret_key = "@S3cr3t_K3y!";//titkosító hash

//modul beállítások
$moduleDir = 'modules/';//modulok mappája
$moduleExt = '.php';//modulfileok kiterjesztése


//menüpontok tömbje
$adminMenu = [
    0 => [
        'title' => 'Vezérlőpult',
        'icon' => 'dashboard',
        'module' => 'dashboard'
    ],
    1 => [
        'title' => 'Cikkek',
        'icon' => 'dashboard',
        'module' => 'articles'
    ],
    10 => [
        'title' => 'Adminisztrátorok',
        'icon' => 'user',
        'module' => 'admins'
    ],
];