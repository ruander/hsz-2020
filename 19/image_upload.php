<?php
include "../15/config/functions.php";
$maxFileSize=10*1024*1024;//10MB
$dir = 'uploads/';
if(!is_dir($dir)){
    mkdir($dir, 0755, true );
}

//echo '<pre>';
if(!empty($_POST)){
    $hiba = [];
    //var_dump($_POST,$_FILES);
    if($_FILES['file']['error'] !== 0 || !is_uploaded_file($_FILES['file']['tmp_name'])){
        $hiba['file'] ='<span class="error">Hiba a feltöltés során!</span>';
    }elseif($_FILES['file']['size']>$maxFileSize ){//file méret
        $hiba['file'] ='<span class="error">A file túl nagy!</span>';
    }else{//kép-e (most kifejezetten csak jpg)
        $valid_image_types = ['image/jpg','image/jpeg' ];//most csak jpg
        $info = getimagesize($_FILES['file']['tmp_name']);//ha nem kép, false!

        if(!$info || !in_array($info['mime'],$valid_image_types)){
            $hiba['file'] = '<span class="error">Nem megfelelő képformátum (jpg)!</span>';
        }
    }
    //file-tipus ellenőrzés kell még

    if(empty($hiba)){
        //file feltöltése
        $originalFileName = $_FILES['file']['name'];
        $fileName = ekezettelenitFileName($originalFileName);
        if(move_uploaded_file($_FILES['file']['tmp_name'],$dir.$fileName)){//eredeti file átmozgatása
            //echo '<h2>Sikeres file feltöltés ide:'. $dir.$fileName.'</h2>';
            //var_dump($info);
            //képműveletek
            //méretarányos kicsinyítés (egy 350x350px dobozba férjen bele)
            $originalWidth = $info[0];
            $originalHeight = $info[1];
            $ratio = $originalWidth/$originalHeight;//képarány

            if($ratio > 1){
                //fekvő kép - landscape
                $targetWidth = 350;
                $targetHeight = round($targetWidth/$ratio);
                //var_dump($targetWidth,$targetHeight);
             }else{
                //álló kép - portrait
                $targetHeight = 350;
                $targetWidth = round($targetHeight*$ratio);
            }
            //var_dump($targetWidth,$targetHeight);
            $canvas = imagecreatetruecolor($targetWidth,$targetHeight);//üres vászon
            $image = imagecreatefromjpeg($dir.$fileName);//eredeti kép memóriába

            imagecopyresampled ( $canvas, $image , 0 , 0 , 0 , 0 , $targetWidth , $targetHeight , $originalWidth , $originalHeight );

            //böngésző beéllítása hogy ez egy kép file
            //header('content-type:image/jpeg');

            //filenév: eredetinev-350.jpg
            $pathinfo = pathinfo($dir.$fileName);
            $fileName = $pathinfo['filename'].'-350.'.$pathinfo['extension'];
            var_dump($pathinfo);
            //kép memóriából ebbe a fileba
            imagejpeg($canvas, $dir.$fileName,100 );//kiírás fileba

            //@todo HF:150x150es thumbnail készítése (imagecrop) filenév: eredetinev-thumb.jpg

            //takarítás - erőforrások felszabadítása
            imagedestroy($canvas);
            imagedestroy($image);
            //exit();
        }


    }
}

?><form method="post" enctype="multipart/form-data">
    <label>
        <input type="file" name="file">
        <?php
        echo getError('file');
        ?>
    </label>
    <button name="submit">mehet</button>
</form>