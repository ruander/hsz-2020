<?php
/*
for(ciklusváltozó kezdeti értéke;belépési feltétel vizsgálata;ciklusváltozó léptetése){
    //ciklusmag
}
 */
//írjuk ki a számokat 1-10ig
for ($i = 1; $i <= 10; $i++) {
    echo "<br>$i";//kiírjuk a ciklusváltozót
}

/*
while(condition == true){
    //ciklusmag
}
 */
//ugyanaz mint fentebb a for
$i = 0;//alapérték
while ($i < 10) {//belépési feltétel
    echo "<br>" . $i++;//kiírás közben léptetjük
    ////léptetjük $i++
}
/*
Hátultesztelő változat
do{
   //ciklusmag
}while(condition);
*/
$testArray = [
    "id" => 5,
    "email" => "hgy@iworkshop",
    "name" => "Horváth György"
];
/*
foreach(:array||:object as $key => $value){
    //ciklusmag
}
*/
foreach ($testArray as $k => $v) {
    //bizonyos feltételnél ...
    if ($k == 'id') continue;
    echo "<br>az aktuális kulcs: $k, az aktuális érték: $v";
    //bizonyos feltételnél lépjünk ki
    if ($k == 'email') break;
}

//tölts fel egy tömböt 10 véletlen számmal 1 és 100 között
$testArray = [];//ide gyüjtjük az elemeket
while (count($testArray) < 10) {
    $testArray[] = rand(1, 100);
}
echo '<pre>' . var_export($testArray, true) . '</pre>';
//for
$testArray = [];
for ($i = 1; $i <= 10; $i++) {
    $testArray[] = rand(1, 100);
}
echo '<pre>' . var_export($testArray, true) . '</pre>';
$testArray = [];
//hacked for
for (; count($testArray) < 10;) {
    $testArray[] = rand(1, 100);
}
echo '<pre>' . var_export($testArray, true) ;//. '</pre>';
//tömb műveletek
var_dump( count($testArray) );//tömb elemszáma :int
var_dump( array_sum($testArray) );//elemek összege :int :float
var_dump( array_reverse($testArray) );//elemsorrend visszafelé :array
var_dump( array_keys($testArray) );//tömbben található kulcsok értékként :array
var_dump( array_values($testArray) );//tömbben található értékek sorrendben auto indexxel :array
var_dump( array_flip($testArray) );
$test = [
    15,
    null,
    6/7,
    "12Gyuri",//string elején ha értelmezhető szám van, az lesz a string értéke, különben 0
    true,//true értéke 1, false :0
    100
];
var_dump($test);
var_dump( array_sum($test) );
var_dump( "12 Majom" + "5 kutya");
//tipuskényszerítés
$test = "12 Majom";
settype($test,"integer");
var_dump($test);
$test = null;
var_dump($test);//string
settype($test,"boolean");
var_dump($test);//boolean