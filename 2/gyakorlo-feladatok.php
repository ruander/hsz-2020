<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gyakorló feladatok</title>
</head>
<body>
<?php
//1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
$a = 231;
$felszin = $a*$a*6;
echo "Egy {$a}m élhosszúságú kocka felülete: {$felszin}m<sup>2</sup>";

//...11-ig HF
//15.Írjon egy programot, amely kiszámolja az 1-től 20-ig terjedő egész számok négyzetét, de csak azokat az értékeket írja ki, amely nagyobb, mint 30. Használjon ciklus utasítást.
$test15 = [];
for($i=1;$i<=20;$i++){
    $szam = $test15[$i] = pow($i,2);
    if($szam > 30){
        echo "<br>$szam";
    }
}
//tömb bejárással
foreach($test15 as $v){
    if($v > 30){
        echo "<br>$v";
    }
}
//16.Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mtpl = 1;//szorzat alapérték, ez nem változtatja a szorzatot
for($i=1 ; $i<20 ; $i+=2 ){//2-esével léptetjük a ciklusváltozót 1 től | $i=$i+2 -> $i+=2
    $mtpl *= $i;//mindig megszorozzuk a ciklusváltozó aktuális értékével az addigi szorzatot | $i=$i*2 -> $i*=2
}
echo "<p>1*3*5*7*9*11*13*15*17*19 = $mtpl</p>";
//innen végig HF (tehát csak a 12,13 NEM HF!)
//19-20.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
//2
//22
//222
//2222
//beágyazott ciklus
$str = "2";
for($i=1;$i<=4;$i++){
    //külső ciklusmag
    //2-esek kiírása - belső ciklus
    for($j=1;$j<=$i;$j++){
        //belső ciklusmag
        echo $str;
    }
    echo "<br>";
}
//ugyanaz str_repeattel
for($i=1;$i<=4;$i++){
    //2-esek kiírása - belső ciklus - most str_repat
    echo str_repeat($str,$i)."<br>";
}
//20 visszafele
for($i=4;$i>=1;$i--){
    //2-esek kiírása - belső ciklus
    for($j=1;$j<=$i;$j++){
        echo $str;
    }
    echo "<br>";
}

?>
</body>
</html>