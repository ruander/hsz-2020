<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Változók 2.</title>
</head>
<body>
<?php
$tomb = [
    "id" => 5,
    "email" => "hgy@iworkshop",
    "name" => "Horváth György"
];
settype($tomb,"object");//a tömb tipusát átállítjuk objektumra - type casting
var_dump(gettype($tomb),$tomb);
//objektum tulajdonság értékének elérése
echo "<br>
        $tomb->email
      <br>
        {$tomb->name}";

//elágazás
//int rand max értéke a rendszerben
//echo '<pre>'.var_export(getrandmax(), true).'</pre>';
$randomNumber = rand(1,6);
/*
if(condition){
    //true
}else{
    //false
}
 */
echo '<br>A $randomNumber értéke: '.$randomNumber.', ami';
if($randomNumber%2 == 0 ){
    //true
     echo ' páros.';//string fűzéssel
}else{
    //false
    //echo "<br>A \$randomNumber értéke: $randomNumber, ami páratlan";//""-el és escape
    echo ' páratlan.';
}
//short hand if
// (condition)?true:false;
echo '<br>A $randomNumber értéke: '.$randomNumber.', ami '. ( ($randomNumber%2 == 0 )?'páros.':'páratlan.' );

/*
switch(változó){
    case 'érték1':
        //kód 1
        break;
    case 'érték2':
        //kód 2
        break;
    ...
    default:
        //default eset kód
        break;
}
...
 */
//6-os lottó nyerőosztály szerint üzenet
switch($randomNumber){
    case 3:
        echo '<h3>kis nyeremény</h3>';
        break;
    case 4:
        echo '<h3>közepes nyeremény!</h3>';
        break;
    case 5:
        echo '<h3>Nagy nyeremény!</h3>';
        break;
    case 6:
        echo '<h3>!!!Telitalálat!!!</h3>';
        break;
    default:
        echo '<h3>Sajnos...</h3>';
        break;
}
?>
</body>
</html>
