<?php
echo "<pre>";
var_dump($_GET);//a GET szuperglobális tömb tartalma
//echo $_GET["szoveg"];//EZT ÍGY SOHA POSTBÓL ÉS GETBŐL!!!!!!!!
//HELYESEN:
echo $szoveg = filter_input(INPUT_GET,"szoveg");
//HTMLTAGEK eltávolítása pl script és hasonló okosságok
$filteredText = strip_tags($szoveg,'<b>');
echo $filteredText;

//POST szuperglobális tömb
var_dump($_POST);

//POST és a GET ( és a COOKIE) elemeit tartalmazza
var_dump($_REQUEST);