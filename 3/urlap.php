<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Űrlapok kezelése PHP -ban</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        form {
            width:450px;
            margin: 0 auto;
            display:flex;
            flex-direction: column;
        }
        label {
            margin: 5px 0;
            display:flex;
            flex-direction: column;
        }
    </style>
</head>
<body>
<h1>Űrlapok</h1>
<h2>GET metódus</h2>
<form method="get" action="feldolgoz.php">
    <label>
        Szöveg: <input type="text" name="szoveg" value="" placeholder="irj be valamit...">
    </label>
    <label>
        email <input type="text" name="email" value="" placeholder="email@cim.hu">
    </label>
    <label>
        jelszó <input type="password" name="pass" value="" placeholder="******">
    </label>
    <button>Mehet</button>
</form>
<h2>POST metódus</h2>
<form method="post" action="feldolgoz.php?email=helloworld">
    <label>
        Szöveg: <input type="text" name="szoveg" value="" placeholder="irj be valamit...">
    </label>
    <label>
        email <input type="text" name="email" value="" placeholder="email@cim.hu">
    </label>
    <label>
        jelszó <input type="password" name="pass" value="" placeholder="******">
    </label>
    <button>Mehet</button>
</form>

</body>
</html>
