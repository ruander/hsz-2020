<?php
    //HF : feladatgyüjtemény.pdf 1-7
    //filter_input: https://www.php.net/manual/en/function.filter-input.php
    //szűrők:https://www.php.net/manual/en/filter.filters.validate.php
//ha nem üres a $_POST akkor nézzük mit kaptunk
if(!empty($_POST)){//operátor ! -> negálás (nem)
    var_dump($_POST);
    //hibakezelés
    $hiba = [];//ide kerülnek a mezőhibák
    //név mező ne legyen üres
    $name = filter_input(INPUT_POST,'name');
    if($name == ""){
        $hiba['name'] = '<span class="error">Név nem lehet üres!</span>';
    }
    //email
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
    echo '<pre>'.var_export($email,true).'</pre>';
    if(!$email){//ha nem jo az email (filter: null  vagy false)
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //jelszó
    var_dump($hiba);
    if(empty($hiba)){
        //üres a hibatömb, minden adat jó... csinálhatjuk a az adatfeldolgozást
    }

}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>űrlap feldolgozása azonos file-ban</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        form {
            width:450px;
            margin: 0 auto;
            display:flex;
            flex-direction: column;
        }
        label {
            margin: 5px 0;
            display:flex;
            flex-direction: column;
        }
        .error {
            color: #f00;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        <span>Név<sup>*</sup>:</span> <input type="text" name="name" value="<?php echo filter_input(INPUT_POST,'name'); ?>" placeholder="irj be valamit...">
        <?php
        if(isset($hiba['name'])){
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        email <input type="text" name="email" value="<?php echo filter_input(INPUT_POST,'email') ?>" placeholder="email@cim.hu">
        <?php
        if(isset($hiba['email'])){
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        jelszó <input type="password" name="pass" value="" placeholder="******">
    </label>
    <button>Mehet</button>
</form>
</body>
</html>
