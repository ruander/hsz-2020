<?php
    //@todo HF: 11es feladatot megpróbálni megoldani.
    // 1: bekérsz egy N számot és utána kiteszel N mezőt és ha minden érték jó, összeadod és kiírod
    // 2: input type="hidden" (name,value) - segítségével megoldahtó a feladat szerinti működés is akár részösszeg kiírással is

if(!empty($_POST)){
    $hiba = [];
    //n mező hibakezelése pozizív egész számokra
    //options tömb segítségével minimum 1 re állitjuk az elfogadott értéket
    $options = [
            "options" => [
                    "min_range" => 1
            ]
    ];
    $n = filter_input(INPUT_POST,'n',FILTER_VALIDATE_INT, $options);//egész szám-e
    //var_dump($n);
    if(!$n){//ha nincs $n, akkor hiba van
        $hiba["n"]='<span class="error">Nem érvényes formátum!</span>';
    }

    if(empty($hiba)){
        //nincs hiba
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Gyakorlás</title>
</head>
<body>
<form method="post">
    <label>
        Adj meg egy egész számot: <input type="text" name="n" value="<?php echo getValue('n');//saját eljárás hívása ?>" placeholder="3" size="2" maxlength="2">
        <?php
        //hibakiírás, ha van
        echo hibaKiir('n');
        ?>
    </label>
    <button>Mehet</button>
</form>
<?php

//ha üres a hibatömb és létezik az n, akkor jo az 'n' érték
if(isset($n) && empty($hiba)){
    //die('létezik az n');//itt megáll a kód futás azonnal és a std outputra kiírja a paraméternek kapott stringet
    //feladat megoldása a már biztosan jó $n értékkel
    //8. Készítsünk programot, amely bekér egy N természetes számot, majd kirajzol a képernyőre egymás mellé N-szer az "XO" betűket és a kiírás után a kurzort a következő sor elejére teszi.
    //megoldás:
    echo '<p>'.str_repeat('XO',$n).'</p>';
    //ugyanaz ciklussal
    echo '<p>';//bekezdés nyitása
    for($i=1;$i<=$n;$i++){
        echo 'XO';
    }
    echo '</p>';//bekezdés zárása

    //9. Egészítsük ki az előző programunkat úgy, hogy az előző kiírás alá írja ki N-szer az "OX" betűket is egymás mellé, majd a kurzort ismét a következő sor elejére tegye. (Az előző ciklus után - NE bele a ciklusba! - tegyünk egy hasonló ciklust, ami most XO helyett OX betűket ír ki.)
    echo '<p>';//bekezdés nyitása
    for($i=1;$i<=$n;$i++){
        echo 'OX';
    }
    echo '</p>';//bekezdés zárása

    //10. Egészítsük ki a programunkat úgy, hogy az előző két sort N-szer ismételje meg a program. (Az előző két egymás utáni ciklust tegyük bele egy külső ciklusba.)
    echo '<p>';//bekezdés nyitása
    for($j=1;$j<=$n;$j++) {//kólső ciklus az nszer 2 sor ismétléshez

        for ($i = 1; $i <= $n; $i++) {//XO sor
            echo 'XO';
        }
        echo '<br>';
        for ($i = 1; $i <= $n; $i++) {//OX sor
            echo 'OX';
        }
        echo '<br>';
    }
    echo '</p>';//bekezdés zárása

}

//egy vizsgált szám prim-e
$test = 2357;

$isPrime = true;
for($i=2;$i<=$test/2;$i++){
    //ha találunk olyan i számot ami maradéknélküli osztója a vizsgált számnak, akkor biztos hogy nem prim
    if($test%$i === 0){
        $isPrime = false;
        //nem kell tovább menni
        break;
    }
}
echo $i;
//az isPrime ha nem billent false-ra, akkor a viszgált szám prím
echo "<h2>A vizsgált szám $test, ami ";
    if($isPrime){
        echo "prím szám";
    }else{
        echo "nem prím szám";
    }
echo "</h2>";


?>
</body>
</html>
<?php

//saját eljárás készítése
/*
function eljarasNeve([param1,param2...]){
    //részprogram
}
1. VOID - nincs visszatérése, elvégzi a "dolgát", általában referenciaátadás történik
2. van visszatérése (1 visszatérési érték) - return ...
 */


/**
 * Mezőérték visszaíráshoz segédeljárás
 * @param $fieldName string
 * @return mixed
 */
function getValue($fieldName){
    $ret = "";
    $ret = filter_input(INPUT_POST,$fieldName);
    return $ret;
}

/**
 * Saját eljárás mezőhibák kiírására ahol a $hiba tömb megbeszélt struktúráját alkalmazzuk, egydimenzios POST esetén
 * @param $fieldName string
 * @return false|string
 */
function hibaKiir($fieldName){
    global $hiba;//az eljárás idejére a $hiba változó globális
    //var_dump($hiba);
    if(isset($hiba[$fieldName])){
        return $hiba[$fieldName];//ha van hiba, térjünk vissza vele
    }
    return false;//ha eddig nem volt return, akkor biztonsági false visszatérés
}