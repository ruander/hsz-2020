<?php
//11. Készítsünk programot, amely beolvas egy N természetes számot, majd billentyűzetről bekér N darab. természetes számot és ezeket a számokat összeadja, majd kiírja az eredményt. (Vegyünk egy változót, amit a program elején kinullázunk. Ehhez a cikluson belül mindig adjuk hozzá az éppen beolvasott számot. A szám beolvasása a ciklusban lehet N-szer ugyanabba a változóba, hiszen miután hozzáadtuk az összeghez, már nincs rá szükségünk, tehát használhatjuk a következő szám beolvasására.)
//@todo - az 1. step után az array sum hibát ad mivel az n en nincs hiba így a hibatömbünk üres, de még számok nincsenek ezért kialakul egy helytelen output és a die miatt megáll a folyamat. HF: javítani, hogy a step 1 után ne lehessen output ha nincsenek jó számok

$step = 1;//itt tartunk
if (!empty($_POST)) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];
    //n hibakezelése
    $options = [
        'options' => [
            'min_range' => 2
        ]
    ];
    $n = filter_input(INPUT_POST, 'n', FILTER_VALIDATE_INT, $options);
    if (!$n) {
        $hiba['n'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {
        //van érvényes N számunk
        $step = 2;//jöhet a 2. lépés (összeadandók)
    }

    //összeadandó számok hibakezelése
    $args = [
        'szam' => [
            'flags' => FILTER_REQUIRE_ARRAY,
            'filter' => FILTER_VALIDATE_INT
        ],
        'n' => [
            'filter' => FILTER_VALIDATE_INT,
            'options' => [
                'min_range' => 2
            ]
        ]
    ];
    $test = filter_input_array(INPUT_POST, $args);
    echo '<pre>' . var_export($test, true) . '</pre>';
    if (is_array($test['szam'])) {//ha vannak számok, bejárjuk őket
        foreach ($test['szam'] as $k => $v) {
            if($v===false){
                //ha az aktális érték false (a szűrés miatt - args) akkor hiba van az adott számnál
                $hiba['szam'][$k]='<span class="error">Nem érvényes formátum!</span>';
            }
        }
    }
    echo '<pre>' . var_export($hiba, true) . '</pre>';

    if (empty($hiba)) {
        //ha minden ok
        $output = 'Az összeg: '.array_sum($test['szam']);
    }
}
//ha van output, akkor van eredmény
if(isset($output)){
    echo $output;
    die();//állj
}
$form = '<form method="post">';//form nyitótag

switch ($step) {
    case 2:
        //számok bekérése összeadáshoz (ciklus)
        for ($i = 1; $i <= $n; $i++) {
            //egy mező
            $form .= '<label>Szám ' . $i . '. 
               <input type="text" name="szam[' . $i . ']" value="' . getValue('szam',$i) . '" placeholder="2" size="4">';
            //hiba kiírás ha van, akkor hozzáfűzzük az űrlaphoz
            if( isset( $hiba['szam'][$i] ) ){
                $form .= $hiba['szam'][$i];
            }
            $form .= '</label>';
        }
        //eltároljuk az n értéket egy rejtett mezőbe
        $form .= '<input type="hidden" name="n" value="' . $n . '">';
        break;
    default:
        //N szám bekérése (step 1)
        $form .= '<label>Hány elemet adjunk össze (min:2)? 
           <input type="text" name="n" value="' . getValue('n') . '" placeholder="2" size="2" maxlength="2">';
//hiba kiírás ha van, akkor hozzáfűzzük az űrlaphoz
        $form .= hibaKiir('n');//előző óráról saját eljárás
        $form .= '</label>';
}


$form .= '<button>Mehet</button>
         </form>';//form zárás és gomb

echo $form;

$style = '<style>
    form {
        display:flex;
        flex-flow:column nowrap;
        max-width:450px;
        margin:0 auto;
    }
    .error {
        color:#f00;
        font-style:italic;
        font-size:.7em;
    }
</style>';

//stílusok kiírása
echo $style;

/**
 * Saját eljárás mezőhibák kiírására ahol a $hiba tömb megbeszélt struktúráját alkalmazzuk, egydimenzios POST esetén
 * @param $fieldName string
 * @return false|string
 */
function hibaKiir($fieldName)
{
    global $hiba;//az eljárás idejére a $hiba változó globális
    //var_dump($hiba);
    if (isset($hiba[$fieldName])) {
        return $hiba[$fieldName];//ha van hiba, térjünk vissza vele
    }
    return false;//ha eddig nem volt return, akkor biztonsági false visszatérés
}

/**
 * Mezőérték visszaíráshoz segédeljárás
 * @param $fieldName string
 * @param null $subkey | ha tömbb az adott elem,akkor a 2. dimenzió kulcsa
 * @return mixed
 */
function getValue( $fieldName, $subkey = null)
{
    $ret = "";
    if($subkey !== null){//kaptunk 2. szint kulcsot
        //szűréssel kell visszaadni az adott elemet
        //órai feladat: szűrés, és a kivánt értékkel visszatérés
            //$_POST[fieldname][subkey] nem használható
        $args = [
            $fieldName => [
                'flags' => FILTER_REQUIRE_ARRAY
            ]
        ];
        $filter = filter_input_array(INPUT_POST, $args);//a szűrt elem már elérhető tombnev[fieldname][subkey], amivel vissza tudsz térni
        //var_dump($filter);
       /* if(is_array($filter[$fieldName])){
            $ret = $filter[$fieldName][$subkey];
        }*/
        //vagy
        if(isset($filter[$fieldName][$subkey])) $ret = $filter[$fieldName][$subkey];

    }else{
        $ret = filter_input(INPUT_POST, $fieldName);
    }
    return $ret;
}