<?php
//mappa és filekezelés
//https://www.php.net/manual/en/book.dir.php
//https://www.php.net/manual/en/book.filesystem.php
//fopen,fread,fwrite,fclose

//mappa létének ellenőrzése
// ./ - ahol vagyok  | ../ - egy mappával feljebb

$dir = 'test/';//ez a mappa neve
if(!is_dir($dir)){
    //készítsük el a mappát
    mkdir($dir, 0755, true );
}

//szövegfile létrehozása, egyszerű módon
$fileContent = 'Ez az új tesztfile tartalma...';
$fileNev = 'testfile.txt';

$kiiras = file_put_contents($dir.$fileNev,$fileContent,FILE_APPEND);
var_dump($kiiras);

//file tartalom beolvasása
$beolvasottTartalom = file_get_contents($dir.$fileNev);
var_dump($beolvasottTartalom);

//példa
$content = '<?php echo "Hello World!"; ';
$fileName = 'index.php';
file_put_contents($fileName,$content);
echo '<a href="index.php" target="_blank">katt</a>';