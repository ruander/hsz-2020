<?php
//http://slimhamdi.net/istanbul/demos/index-dark.html#
//oldal paraméter az urlből
$o = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//vagy kapunk paramétert vagy legyen 0, azaz home
/*if(feltétel){
    igaz ág
}else{
    hamis ág
}
rövid if
(feltétel) ? igaz ág : hamis ág;
még rövidebb ha a feltétel maga az igaz ág is
(feltétel)?:hamis ág;
*/
//menü tömb kialakítása
/*
 * Egy menüpont mutat egy linkre, van ikonja, van 'sorszáma' (azonosító),címe
 */
$menuItems = [
    0 => [
        'title' => 'home',
        'icon' => 'fas fa-home',
        'url' => 'https://'
    ],
    1 => [
        'title' => 'about',
        'icon' => 'fas fa-user',
        'url' => 'https://'
    ],
    2 => [
        'title' => 'portfolio',
        'icon' => 'fas fa-briefcase',
        'url' => 'https://'
    ],
    3 => [
        'title' => 'contact',
        'icon' => 'fas fa-envelope',
        'url' => 'https://'
    ],
    4 => [
        'title' => 'blog',
        'icon' => 'fad fa-comments',
        'url' => 'https://'
    ],
    5 => [
        'title' => 'teszt',
        'icon' => 'fab fa-facebook',
        'url' => 'https://'
    ],
];
//menü
$menu = '<nav>
            <ul class="mainMenu">
                <li class="menuToggle"><i class="fas fa-bars"></i></li>';//menü nyitás és az első elem ami kell és nem a tömbből jön
//menüpontok a tömb bejárásával
foreach ($menuItems as $menuId => $menuItem) {
    //ha az urlből kapott érték egyezik az aktuális menuId-val akkor az az aktiv menüpont :)
    $active = ($menuId == $o)? 'active':'';
    $menu .= '<li class="'.$active.'">
            <a href="?p=' . $menuId . '">
                <span class="icon"><i class="' . $menuItem['icon'] . '"></i></span>
                <span class="divider"></span>
                <span class="title">' . $menuItem['title'] . '</span>
            </a>
        </li>';
}

//menü zárása
$menu .= '</ul>
</nav>';
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bonyolultabb menü minta bemutatása</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        ul, ol {
            list-style: none;
        }

        html, body {
            height: 100%;
        }

        body {
            font-family: "Trebuchet MS", Arial, sans-serif;
            background-color: #111;
            font-size: 14px;
        }

        a {
            text-decoration: none;
            color: #fff;
        }

        ul.mainMenu {
            /*background-color: lime;*/
            margin-left: 45px;
            margin-top: 45px;
            display: inline-flex;
            flex-direction: column;
            align-items: flex-start;
            color: #ddd;
        }


        ul.mainMenu > li {
            /*background-color:pink;*/
            padding: 10px 15px;
        }

        ul.mainMenu > li > a {
            display: flex;
            text-transform: uppercase;
        }

        ul.mainMenu > li:hover > a,
        ul.mainMenu > li.active > a {
            color: orange;
        }

        ul.mainMenu > li > a > .icon {
            margin: -15px 0 -15px -15px;
            background-color: #666;
            padding: 15px;
            flex: 1 0 50px;
            text-align: center;
        }

        ul.mainMenu > li > a > .icon > i,
        ul.mainMenu > li {
            transition: padding-top 0.3s ease-in-out,
            padding-bottom 0.3s ease-in-out,
            height 0.3s ease-in-out;
        }

        ul.mainMenu > li.active > a > .divider {
            background-color: orange;
            flex: 0 0 2px;
            margin-left: -2px;
        }

        ul.mainMenu > li > a > .title {
            margin-left: 20px;
            transition: all 0.3s ease-in-out;
        }

        ul.mainMenu > li:hover > a > .title {
            margin-left: 25px;
        }

        ul.mainMenu > li.menuToggle {
            background-color: #666;
            padding-bottom: 5px;
            padding-top: 20px;
            width: 50px;
            border-top-right-radius: 50px;
            border-top-left-radius: 50px;
            text-align: center;
        }

        ul.mainMenu:after {
            content: "";
            display: block;
            background-color: #666;
            padding: 0 0 25px;
            width: 50px;
            border-bottom-left-radius: 50px;
            border-bottom-right-radius: 50px;
        }

        /*csukott állapot*/
        ul.mainMenu.closed > li:not(.menuToggle) {
            height: 0;
            padding-top: 0;
            padding-bottom: 0;
            overflow: hidden;
            /*display: none;*/
        }

        ul.mainMenu.closed > li.menuToggle {
            height: 25px;
            position: relative;
            padding: 15px 15px 0;
            line-height: 22px;
        }
    </style>
</head>
<body>
<?php
//menü kiírása
echo $menu;
?>

<script>
    let menuToggle = document.querySelector("ul.mainMenu > .menuToggle");
    let menu = document.querySelector("ul.mainMenu");
    //menu.classList.add("closed");//alapból becsukjuk a menüt
    //gomb
    menuToggle.onclick = function () {
        let icon = document.querySelector("ul.mainMenu > .menuToggle > i");

        if (menu.classList.contains("closed")) {
            menu.classList.remove("closed");
            //kinyitjuk, hamburger csere X re
            icon.classList.add("fa-times");
            icon.classList.remove("fa-bars");
        } else {
            menu.classList.add("closed");
            //becsukjuk: X csere hamburgerre
            icon.classList.add("fa-bars");
            icon.classList.remove("fa-times");

        }
    }
</script>
</body>
</html>