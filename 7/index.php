<?php
//érvényes játéktipusok tömbje
include 'settings.php';
//menü a játéktipusok linkjeivel
$menu = '<ul>';
//valós játékt. bejárása
foreach(VALID_GAME_TYPES as $tippek_szama => $limit){
    $menu .= '<li><a href="lotto.php?game='.$tippek_szama.'">'.$tippek_szama.'/'.$limit.' lottójáték</a></li>';
}
$menu .= '</ul>';
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték - játéktípus kiválasztása</title>
</head>
<body>
<h1>Lottójáték - játéktípus kiválasztása</h1>
<?php
//választómenü kiírása
echo $menu;
?>
</body>
</html>