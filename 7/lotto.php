<?php
//settings betöltése, mintha ide lenne gépelve
include 'settings.php';
//require 'settings.php';
//include_once 'settings.php';//csak ha még nem volt betöltve az adott file
//require_once 'settings.php';//csak ha még nem volt betöltve az adott file
//erőforrások
$tippek_szama = filter_input(INPUT_GET, 'game', FILTER_VALIDATE_INT)?:0;

//rendben vannak e az erőforrások
if (!array_key_exists($tippek_szama, VALID_GAME_TYPES)) {
    header('location:index.php');//átirányítás a választómenühöz
    exit();//die('Gáz van!');
} else {
    $limit = VALID_GAME_TYPES[$tippek_szama];
}
if (!empty($_POST)) {
    //hibakezelés
    $hiba = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //név mező
    $name = filter_input(INPUT_POST, 'name');
    //spacek eltávolítása
    $name = trim($name);
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['name'] = '<span class="error">Minimum 3 karakter!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //eredeti tippek a későbbi műveletekhez
    $args = [
        'tippek' => [
            'flags' => FILTER_REQUIRE_ARRAY
        ]
    ];
    $eredeti_tippek = filter_input_array(INPUT_POST, $args);
    //echo '<pre>' . var_export($eredeti_tippek, true) . '</pre>';
    //egyedi tippek
    $egyedi_tippek = array_unique($eredeti_tippek['tippek']);
    //echo '<pre>' . var_export($egyedi_tippek, true) . '</pre>';
    //tippek hibakezelése
    $args = [
        'tippek' => [
            'flags' => FILTER_REQUIRE_ARRAY, //mert tömbnek kell lenniük, mást nem fogadunk el
            'filter' => FILTER_VALIDATE_INT, //mert egész zámnak kell lennük
            'options' => [
                'min_range' => 1,
                'max_range' => $limit
            ]//mert ezek közé kell kerülni a kapott értékeknek
        ]
    ];
    $szures = filter_input_array(INPUT_POST, $args);
    //echo '<pre>' . var_export($szures, true) . '</pre>';
    //a szures tippek kulcsán egy tömb adja vissza a szűrés eredményét
    //ez a halmaz bejárható és a helytelen értékeknél kialakítjuk a hibaüzenezt a hiba tömbben
    if (is_array($szures['tippek'])) {
        foreach ($szures['tippek'] as $sorszam => $tipp) {
            if (!$tipp) {//hibauzenet 1.
                $hiba['tippek'][$sorszam] = '<span class="error">Nem érvényes formátum!</span>';
            } elseif (!array_key_exists($sorszam, $egyedi_tippek)) {
                //ha nincs az egyedi tippekben a kulcsa az adott elemnek akkor kikerült duplikáció miatt,azaz ismétlődő tipp
                $hiba['tippek'][$sorszam] = '<span class="error">Ez a tipp már szerepelt!</span>';
            }
        }//ciklus
    }//ha tömb a tippek
    //echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        //művelet
        //adatok tisztázása
        sort($egyedi_tippek);//emelkedő számsorrend
        $data = [
          'name' => $name,
          'email' => $email,
          'szelveny' =>  $egyedi_tippek,
          'datum' => date('Y-m-d H:i:s')
        ];
        //echo '<pre>' . var_export($data, true) . '</pre>';
        //mappa és filerendszer: tippek/(játéktipus[5,6,7]/(éééé)/(hh).json
        $dir = 'tippek/'.$tippek_szama.'/'.date('Y').'/';//ebbe a mappába dolgozunk

        if(!is_dir($dir)){//mappa ellenőrzés/létrehozás
            mkdir($dir, 0755, true);
        }
        //Aktuális hét száma
        $het = date('W');
        $extension = '.json';
        $fileNameWithDirname = $dir.$het.$extension;
        //echo $fileNameWithDirname;
        //eddigi szelvények
        $szelvenyek = [];//szelvények halmaza amit ber tudunk olvasni, vagy üres halmaz
        if(file_exists($fileNameWithDirname)){
            $readFromFile = file_get_contents($fileNameWithDirname);
            $szelvenyek = json_decode($readFromFile, true);
        }
        $szelvenyek[] = $data;//ha már volt leadott szelvény akkor azokat ki kell egészíteni az új halmazzal, ha ez az első szelvény akkor ő lesz a halmaz első eleme

        //data átalakítása a kívánt formátumra, most json
        $dataJson = json_encode($szelvenyek);//összes szelvény jsonre alakítása
        //echo "<pre>$dataJson";
        //fileba írás
        file_put_contents($fileNameWithDirname,$dataJson);

        //visszalakítás...
        //var_dump(json_decode($dataJson, true));
        //$ujraData = json_decode($dataJson, true);
        //echo '<pre>' . var_export($ujraData, true) . '</pre>';
        //if( $data === $ujraData ) echo 'Minden egyezik!';//ellenőrzés, az uj verzióban már nem egyeznek mert a data 1 szelvény a ujraData meg szelvényhalmaz
        header('location:index.php');//vissza a játékválasztóba
        exit;
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 450px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0 0;
        }

        .error {
            font-size: 0.7em;
            font-style: italic;
            color: #f00;
            padding-top: 3px;
        }
    </style>
</head>
<body>
<form method="post" class="szelveny">
    <h1><?php echo "$tippek_szama/$limit"; ?> Lottójáték</h1>
    <section class="personal-info">
        <h2>Személyes adatok</h2>
        <label>
            Név
            <input type="text" name="name" placeholder="Minta János" value="<?php echo getValue('name') ?>">
            <?php
            echo hibaKiir('name');
            ?>
        </label>
        <label>
            Email
            <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo getValue('email') ?>">
            <?php
            echo hibaKiir('email');
            ?>
        </label>
    </section>
    <section class="tippek">
        <h2>Tippsor</h2>
        <?php
        //ciklus a tippeknek
        $fields = '';//itt lesznek az inputok
        for ($i = 1; $i <= $tippek_szama; $i++) {
            $fields .= '<label>
            Tipp ' . $i . '. <input type="text" name="tippek[' . $i . ']" size="1" maxlength="2" value="' . getValue('tippek', $i) . '" placeholder="' . $i . '">';//mező
            //mezőhiba
            $fields .= hibaKiir('tippek', $i);//az aktuális sorszámú mező hibája
            $fields .= '</label>';
        }//for ciklus vége
        echo $fields;//mezők űrlapba írása
        ?>
    </section>
    <button>Tippsor beküldése</button>
</form>
</body>
</html>
<?php
/**
 * Mezőérték visszaíráshoz segédeljárás
 * @param $fieldName string
 * @param string $subKey
 * @return mixed
 */
function getValue($fieldName, $subKey = '')
{
    $ret = "";
    //ha van subkey, akkor több dimenziós a post, alapszűréssel visszaadjuk az adott elemet
    if ($subKey != '') {
        $args = [
            $fieldName => [
                'flags' => FILTER_REQUIRE_ARRAY,
            ]
        ];
        $szures = filter_input_array(INPUT_POST, $args);
        if (isset($szures[$fieldName][$subKey])) {
            $ret = $szures[$fieldName][$subKey];//visszatérés kialakítása tömb esetén
        }
    } else {
        $ret = filter_input(INPUT_POST, $fieldName);//viszzatérés kialakítása primitiv esetén
    }

    return $ret;
}

/**
 * Saját eljárás mezőhibák kiírására ahol a $hiba tömb megbeszélt struktúráját alkalmazzuk, egydimenzios POST esetén
 * @param $fieldName string
 * @param string $subKey
 * @return false|string
 */
function hibaKiir($fieldName, $subKey = '')
{
    global $hiba;//az eljárás idejére a $hiba változó globális
    //var_dump($hiba);
    //ha van subkey, akkor 2 dimenziós a hibatömb
    /*if($subKey != ''){

    }*/
    //ha tömb elem van a fielsName-n
    if (isset($hiba[$fieldName][$subKey]) && is_array($hiba[$fieldName])) {
        return $hiba[$fieldName][$subKey];
    } elseif (isset($hiba[$fieldName]) && !is_array($hiba[$fieldName])) {
        return $hiba[$fieldName];//ha van hiba, térjünk vissza vele
    }
    return false;//ha eddig nem volt return, akkor biztonsági false visszatérés
}