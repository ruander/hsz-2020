<?php
//ez a file az 5/90 2020/51. heti szelvényeket olvassa be
$fileNameWithDirname = 'tippek/5/2021/01.json';
if(!file_exists($fileNameWithDirname)){
    die('nincs mit beolvasni');
}

$readFromFile = file_get_contents($fileNameWithDirname);
$szelvenyek = json_decode($readFromFile, true);
//echo '<pre>'.var_export($szelvenyek, true).'</pre>';
//táblázat fejléc
$table = '<table><tr>
    <th>szelvény \'azonositó\'</th>
    <th>név</th>
    <th>email</th>
    <th>szelvény</th>
    <th>dátum</th>
</tr>';
//ciklus a táblázat sorainak
foreach($szelvenyek as $szelvenyId => $szelveny){
    $table .= '<tr>
                    <td>'.$szelvenyId.'</td>
                    <td>'.$szelveny['name'].'</td>
                    <td>'.$szelveny['email'].'</td>
                    <td>'.implode(',',$szelveny['szelveny'])/*implode -> tömb->string*/.'</td>
                    <td>'.$szelveny['datum'].'</td>
                </tr>';
}
//táblázat zárás
$table .= '</table>';
//táblázat kiírása
echo $table;