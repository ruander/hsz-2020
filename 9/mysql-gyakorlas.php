<?php
//mysql adatbázis csatlakozás adatai
$DBhost = 'localhost';
$DBuser = 'root';
$DBpass = '';
$DBname = 'classicmodels';

//db csatlakozás
$link = @mysqli_connect($DBhost,$DBuser,$DBpass,$DBname) or die(mysqli_connect_error())  ;
//a $link változó tartalmazza az erőforrást amin DB műveleteket tudunk alkalmazni
//var_dump($link);

//db kapcsolat bezárása, de csak akkor zárd ha mindent elintéztél amit akartűl a DB ben
//mysqli_close($link);