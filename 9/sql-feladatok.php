<?php
require "mysql-gyakorlas.php";//adatbázis csatlakozás betöltése

//var_dump($link);

//1. Mennyi az összes raktáron lévő hajómodel értéke (buyprice)?
/*
SELECT
	SUM(buyprice*quantityInStock)
FROM products
WHERE productline = 'ships'
 */

$qry = "SELECT
            SUM(buyprice*quantityInStock)
        FROM products
        WHERE productline = 'ships'";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés vagy állj!

//feldolgozás - kibontás
$row = mysqli_fetch_row($result);

//var_dump($row);
//a válasz a kérdésre:
echo "<div>A raktáron lévő hajómodellek értéke: USD $row[0]</div>";

//alkalmazottak listája
/*
SELECT CONCAT(firstname,' ',lastname) FROM employees
 */
$qry = "SELECT CONCAT(firstname,' ',lastname) 'név' FROM employees";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
/*var_dump($result);
$row = mysqli_fetch_row($result);//egy sor kibontása
var_dump($row);
$row = mysqli_fetch_assoc($result);//egy sor kibontása
var_dump($row);
$row = mysqli_fetch_array($result);//egy sor kibontása
var_dump($row);
$row = mysqli_fetch_object($result);//egy sor kibontása
var_dump($row);
//összes elem kibontása egyszerre
$rows = mysqli_fetch_all($result);
var_dump($rows);

var_dump(mysqli_fetch_all($result));*/
//kapott sorok száma
//var_dump(mysqli_num_rows($result));
/*for($i=0 ; $i<mysqli_num_rows($result) ; $i++){
    $row = mysqli_fetch_row($result);
    var_dump($row[0]);
}*/
$output = '<h2>alkalmazottak listája</h2>
            <ul>';//cím és lista nyitása
//kibontás while ciklussal, amig tudunk kibontunk
while( NULL !== $row = mysqli_fetch_row($result) ){
    //var_dump($row[0]);
    $output .= '<li>'.$row[0].'</li>';//listaelem hozzáadása az outputhoz
}

$output .= '</ul><pre>';//lista zárása

echo $output;//kiírás egy lépésben

//19. Alkalmazott teljes neve, főnöke teljes nev , ha nincs neki akkor 'Boss'
$qry = "SELECT CONCAT(firstname,' ',lastname),reportsto FROM employees";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
while( NULL !== $row = mysqli_fetch_row($result) ){
    var_dump($row[0]);//név
    var_dump($row[1]);//főnök azonosítója
    //kérjük le a főnököt is, ha az létezik, egyébként állitsuk be, hogy BOSS
    if($row[1] == NULL){
        $rowFonok[0] =  'BOSS';
    }else {
        $qry = "SELECT CONCAT(firstname,' ',lastname) FROM employees WHERE employeenumber  = $row[1] LIMIT 1";
        $resultFonok = mysqli_query($link, $qry) or die(mysqli_error($link));//lekérjük a főnököt
        $rowFonok = mysqli_fetch_row($resultFonok);
    }

    //a két adat kiírása
    var_dump($rowFonok[0]);
}
//Ha SQLben oldunk meg mindent amit lehet
/*
SELECT
	CONCAT(e.firstname,' ',e.lastname) alkalmazott,
    IF(e2.firstname IS NULL, 'BOSS', CONCAT(e.firstname,' ',e.lastname)) fonok
FROM employees e
LEFT JOIN employees e2
ON
e.reportsto = e2.employeeNumber
 */
//HF táblázatban legyenek az adatok

//5. Irodánként mennyit (összeg) rendeltek?
/*SELECT
	off.country,
    off.city,
    SUM(quantityordered*priceeach) osszeg
FROM offices off
LEFT JOIN employees e
ON off.officecode = e.officeCode
LEFT JOIN customers c
ON c.salesRepEmployeeNumber = e.employeeNumber
LEFT JOIN orders o
ON o.customerNumber = c.customerNumber
LEFT JOIN orderdetails od
ON od.orderNumber = o.orderNumber
GROUP BY off.officeCode*/
//db kapcsolat bezárása
